%global git0 https://github.com/rootless-containers/%{name}
%global commit0 c4e1bc5a5e6987f3a352ca524f13320a2d483398
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

Name: slirp4netns
Version: 0.1
Release: 5.dev.git%{shortcommit0}%{?dist}
# no go-md2man in ix86 and ppc64
ExcludeArch: %{ix86} ppc64
Summary: slirp for network namespaces
License: GPLv2
URL: %{git0}
Source0: %{git0}/archive/%{commit0}/%{name}-%{shortcommit0}.tar.gz
Patch0: slirp4netns-CVE-2019-14378.patch
Patch1: slirp4netns-CVE-2020-7039.patch
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: gcc
BuildRequires: git
BuildRequires: go-md2man
BuildRequires: make

%description
User-mode networking for unprivileged network namespaces.

%package devel
Summary: %{summary}
BuildArch: noarch

%description devel
%{summary}

This package contains library source intended for
building other packages which use import path with
%{import_path} prefix.

%prep
%autosetup -Sgit -n %{name}-%{commit0}

%build
./autogen.sh
./configure --prefix=%{_usr} --libdir=%{_libdir}
%{__make} generate-man

%install
make DESTDIR=%{buildroot} install install-man

%check

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license COPYING
%doc README.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1.gz

%changelog
* Thu Jan 16 2020 Jindrich Novy <jnovy@redhat.com> - 0.1-5.dev.gitc4e1bc5
- backport fix for CVE-2020-7039
- Resolves: #1791578

* Thu Nov 28 2019 Jindrich Novy <jnovy@redhat.com> - 0.1-4.dev.gitc4e1bc5
- actually add CVE-2019-14378 patch to dist-git
- Related: RHELPLAN-25139

* Fri Sep 27 2019 Jindrich Novy <jnovy@redhat.com> - 0.1-3.dev.gitc4e1bc5
- Fix CVE-2019-14378 (#1768394).

* Fri Nov 16 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.1-2.dev.gitc4e1bc5
- changed summary

* Fri Aug 10 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.1-1.dev.gitc4e1bc5
- First package for RHEL 8
- import from Fedora rawhide
- Exclude ix86 and ppc64
